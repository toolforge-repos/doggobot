# Fixes P820 (arXiv classification) constraint violations on Wikidata
# By Frostly
# Licensed AGPL 3.0

import pywikibot

site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()

# Loop through items
with open('violations.txt', 'r') as file:
    for line in file:
        qid = line.strip()
        print(qid)
    item = pywikibot.ItemPage(repo, qid)

    item_dict = item.get()
    clm_dict = item_dict["claims"]
    clm_list = clm_dict["P818"]

    # Only proceed if there is only one arXiv ID value
    if len(clm_list) == 1:
        for clm in clm_list:
            clm_trgt = clm.getTarget()
            print(clm_trgt)
    else:
        print("Item has more than one or no arXiv ID")

    # Get classifications
    clm_list = clm_dict["P820"]
    for clm in clm_list:
        clm_trgt = clm.getTarget()
        print(clm_trgt)
        # Copy as qualifier
        for claim in item.claims['P818']:
            qualifier = pywikibot.Claim(repo, u'P820')
            target = clm_trgt
            qualifier.setTarget(target)
            claim.addQualifier(qualifier, summary=u'Fixing arXiv classification constraint violation (bot)')

    # Remove old statement form
    for claim in item.claims['P820']:
        item.removeClaims(claim, summary=u'Fixing arXiv classification constraint violation (bot)')
